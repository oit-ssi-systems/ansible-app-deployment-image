FROM vault:1.5.0 as vault
FROM ubuntu:18.04

COPY requirements.txt /
COPY scripts/ /scripts

ENV VAULT_ADDR=https://vault-systems.oit.duke.edu

# hadolint ignore=DL3008,DL3005
RUN apt-get update && \
    apt-get -y install --no-install-recommends \
      software-properties-common sudo python3-pip python-pip \
      openssh-client python3-yaml python3-requests sudo && \
    add-apt-repository ppa:ansible/ansible && \
    apt-get update && \
    apt-get -y install --no-install-recommends \
      ansible && \
    rm -rf /var/lib/apt/lists/* && \
    pip3 install -r ./requirements.txt && \
    useradd -m bumblebeecornwallis && \
    echo 'bumblebeecornwallis ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

COPY --from=vault bin/vault /usr/local/bin/


USER bumblebeecornwallis
