#!/usr/bin/env python3
import sys
import hvac
import logging
import argparse
import os
import subprocess


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="Generate a signed vault key")
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    parser.add_argument("policy", help="Name of ssh policy")

    return parser.parse_args()


def main():
    args = parse_args()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    # Vault connection with local token
    vault = hvac.Client(url=os.environ.get("VAULT_ADDR"))
    if "VAULT_TOKEN" in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ["VAULT_TOKEN"]

    pub_key = os.path.expanduser("~/.ssh/id_ed25519.pub")
    pub_key_signed = os.path.expanduser("~/.ssh/id_ed25519.pub.signed")
    priv_key = os.path.expanduser("~/.ssh/id_ed25519")

    if not os.path.exists(priv_key):
        print("Creating ed25519 key")
        subprocess.check_call(["ssh-keygen", "-t", "ed25519", "-N", "", "-f", priv_key])

    with open(pub_key, "r") as ed25519_pub:
        pub_key = ed25519_pub.read()
        res = vault.write(
            "ssh-client-signer/sign/%s" % (args.policy), public_key=pub_key
        )["data"]["signed_key"]
        with open(pub_key_signed, "w") as ed25519_pub_signed:
            ed25519_pub_signed.write(res)
        os.chmod(pub_key_signed, 0o600)

    return 0


if __name__ == "__main__":
    sys.exit(main())
